import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Customers from '../pages/customers';
import Dashboard from '../pages/dashboard';
import Home from '../pages/index';
import Login from '../pages/login';
import { CreateOrganizations } from '../pages/organizations/createOrganizations';


const AppRoutes = () => {
  return <Router>
    <Routes>
      <Route element={<Home />} path="/"/>
      <Route element={<Login />} path="/login"/>
      <Route element={<Dashboard />} path="/dashboard" />
      <Route element={<Customers />} path="/customers" />
      <Route element={<CreateOrganizations />} path="/map/organizations" />
    </Routes>
  </Router>
}

export default AppRoutes;