import { Box, IconButton, useColorMode } from '@chakra-ui/react';
import React from 'react';
import { RiMoonLine, RiSunLine } from 'react-icons/ri';


export default function ThemeToggler() {
  const { colorMode, toggleColorMode } = useColorMode();
  
  return (
    <Box textAlign="right" py={4} mr={12}>
      <IconButton
        aria-label='Toggle theme mode'
        icon={colorMode === 'light' ? <RiMoonLine /> : <RiSunLine />}
        onClick={toggleColorMode}
        variant="ghost"
      />
    </Box>
  );
}