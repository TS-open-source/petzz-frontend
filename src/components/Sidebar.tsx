import { Flex, Link, Text, VStack } from "@chakra-ui/react";
import { RiDashboardLine, RiHomeSmileLine, RiLogoutBoxLine, RiSettings2Line } from "react-icons/ri";

export function Sidebar() {
  return (
    <Flex
      as="aside"
      w="72"
      bgColor="white"
      py="8"
      mx="6"
      shadow="0 0 20px rgba(0, 0, 0, 0.05)"
      borderRadius={4}
      direction="column"
    >
      <VStack spacing="4" pr="8" alignItems="stretch">
        <Text fontWeight="bold" color="gray.700" fontSize="small" px={8}>GERAL</Text>
        <Link display="flex" alignItems="center" color="green.500" py="1" pl={8} borderLeft="3px solid" href="/dashboard">
          <RiDashboardLine size="20" />
          <Text ml="4" fontSize="medium" fontWeight="medium">Painel</Text>
        </Link>
        {/* <Link display="flex" alignItems="center" py="1" pl={8} color="gray.500" borderLeft="3px solid transparent">
          <RiMailOpenLine size="20" />
          <Text ml="4" fontSize="medium" fontWeight="medium">Mensagens</Text>
        </Link> */}
        {/* <Link display="flex" alignItems="center" py="1" pl={8} color="gray.500" borderLeft="3px solid transparent" href="/customers">
          <RiContactsLine size="20" />
          <Text ml="4" fontSize="medium" fontWeight="medium">Clientes</Text>
        </Link> */}
        <Link href="/map/organizations" display="flex" alignItems="center" py="1" pl={8} color="gray.500" borderLeft="3px solid transparent">
          <RiHomeSmileLine size="20" />
          <Text ml="4" fontSize="medium" fontWeight="medium">Onganizações</Text>
        </Link>
      </VStack>
      <VStack spacing="4" pr="8" mt={8} alignItems="stretch">
        <Text fontWeight="bold" color="gray.700" fontSize="small" px={8}>SISTEMA</Text>
        <Link display="flex" alignItems="center" py="1" pl={8} color="gray.500" borderLeft="3px solid transparent">
          <RiSettings2Line size="20" />
          <Text ml="4" fontSize="medium" fontWeight="medium">Configuração</Text>
        </Link>
      </VStack>
      <VStack spacing="4" pr="8" mt={8} alignItems="stretch">
        <Link display="flex" alignItems="center" py="1" pl={8} color="gray.500" borderLeft="3px solid transparent" mt="auto">
          <RiLogoutBoxLine size="20" />
          <Text ml="4" fontSize="medium" fontWeight="medium">Logout</Text>
        </Link>
      </VStack>
    </Flex>
  );
}