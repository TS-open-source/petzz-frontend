import { Avatar, Box, Flex, Text, useColorMode } from "@chakra-ui/react";
import ThemeToggler from "./ThemeToggller";


export function Header() {

  const { colorMode, toggleColorMode } = useColorMode()

  return (
    <Flex
      as="header"
      h="20"
      bgColor="white"
      px="8"
      shadow="0 0 20px rgba(0, 0, 0, 0.05)"
      alignItems="center"
    >
        <Box >
        <ThemeToggler />
        </Box>
      <Flex alignItems="center" w="60" mr="4" position="absolute" right={0}>

        <Box mr="4">
          <Text fontWeight="medium">Antônio Silva</Text>
          <Text color="gray.500" fontSize="small">tonybsilvadev@gmail.com</Text>
        </Box>
        <Avatar size="md" name="Antonio Silva" src="https://avatars.githubusercontent.com/u/54373473?s=460&u=5325a755528a2e2a6d25f4aab312b361861884bb&v=4">
          {/* <AvatarBadge borderColor="papayawhip" bg="green.500" boxSize="1.25rem" /> */}
        </Avatar>
      </Flex>
      {/* <Flex as="label" cursor="text" flex="1" ml="8" alignSelf="center" color="green.500">
        <RiSearchLine size="20" />
        <Input 
          variant="unstyled"
          color="gray.700"
          placeholder="Buscar por nome, cpf, tag, etc..." 
          paddingX="4"
        />
      </Flex> */}
    </Flex>
  );
}