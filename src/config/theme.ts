import { extendTheme } from '@chakra-ui/react';
import { config } from './';

export const theme = extendTheme({
  config,
  initialColorMode: 'light',
  useSystemColorMode: true,
  fonts: {
    heading: "Inter, sans-serif",
    body: "Inter, sans-serif",
  },
  styles: {
    global: {
      body: {
        bg: 'gray.50'
      }
    }
  }
})