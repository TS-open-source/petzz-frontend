import { Box, Divider, Flex, SimpleGrid, Text } from '@chakra-ui/react';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { MapContainer, Marker, Popup, TileLayer } from 'react-leaflet';
import { Header } from '../../components/Header';
import { Sidebar } from '../../components/Sidebar';

interface IBGEUFResponse {
  sigla: string;
}

interface IBGECityResponse {
  nome: string;
}

export function CreateOrganizations() {

  const [ufs, setUfs] = useState<string[]>([]);
  const [cities, setCities] = useState<string[]>([]);
  const [initialPosition, setInitialPosition] = useState<[number, number]>([0, 0]);
  const [initialLatitude, setInitialLatitude] = useState(0);
  const [initialLongitude, setInitialLongitude] = useState(0);
  const [selectedUf, setSelectedUf] = useState('0');
  const [selectedCity, setSelectedCity] = useState('0');

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(position => {
      const { latitude, longitude } = position.coords;
      setInitialPosition([latitude, longitude]);
      setInitialLatitude(latitude);
      setInitialLongitude(longitude);
    })
  }, []);

  useEffect(() => {
    axios.get<IBGEUFResponse[]>('https://servicodados.ibge.gov.br/api/v1/localidades/estados?orderBy=nome').then(response => {
      const ufInitials = response.data.map(uf => uf.sigla)
      setUfs(ufInitials);
    });
  }, []);


  useEffect(() => {
    // Carregar as cidades sempre que a UF mudar 
    if (selectedUf === '0') {
      return;
    }
    axios.get<IBGECityResponse[]>(`https://servicodados.ibge.gov.br/api/v1/localidades/estados/${selectedUf}/municipios?orderBy=nome`).then(response => {
      const cityNames = response.data.map(city => city.nome)
      setCities(cityNames);
    });
  }, [selectedUf]);

  return (
    <Flex direction="column" h="100vh">
      <Header />
      <Flex my="6" flex="1">
        <Sidebar />
        <SimpleGrid
          flex="1"
          mr="8"
          p="8"
          borderRadius={4}
          columns={1}
          gap="4"
          alignItems="flex-start"
        >
          <Box
            as="div"
            bgColor="white"
            p="0"
            shadow="0 0 20px rgba(0, 0, 0, 0.05)"
            borderRadius={4}
            width={"100%"}
            height={"100%"}
          >
            <Text fontWeight="medium" fontSize="lg">Maps</Text>
            <Divider my="4" />

            <Box as='div' height={"100%"} width={"100%"}>

              {/* {initialPosition} */}

              <MapContainer center={[initialLatitude, initialLongitude]} zoom={7} scrollWheelZoom={false} style={{ height: '100vh' }}>
                <TileLayer
                  attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                  url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Marker position={[initialLatitude, initialLongitude]}>
                  <Popup>
                    A pretty CSS3 popup. <br /> Easily customizable.
                  </Popup>
                </Marker>
              </MapContainer>

            </Box>
          </Box>
        </SimpleGrid>
      </Flex>
    </Flex>
  )
}