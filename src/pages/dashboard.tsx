import { Box, Divider, Flex, SimpleGrid, Text } from '@chakra-ui/react'
import { Header } from '../components/Header'
import { Sidebar } from '../components/Sidebar'

export default function Dashboard() {
  return (
    <Flex direction="column" h="100vh">
      <Header />
      <Flex my="6" flex="1">
        <Sidebar />
        <SimpleGrid 
          flex="1"
          mr="8"
          borderRadius={4}
          columns={3} 
          gap="4"
          alignItems="flex-start"
        >
          <Box
            as="div" 
            bgColor="white" 
            p="8"
            shadow="0 0 20px rgba(0, 0, 0, 0.05)"
            borderRadius={4}
          >
            <Text fontWeight="medium" fontSize="lg">Clientes da semana</Text>
            <Divider my="4" />
            
          </Box>
          <Box
            as="div" 
            bgColor="white" 
            p="8"
            shadow="0 0 20px rgba(0, 0, 0, 0.05)"
            borderRadius={4}
          >
            <Text fontWeight="medium" fontSize="lg">Teste de componente 
              <Text fontWeight="normal" fontSize="sm" display="inline" color="gray.500">
                {' '}(por Produto)
              </Text>
            </Text>
            <Divider my="4" />
          </Box>
          <Box
            as="div" 
            bgColor="white" 
            p="8"
            shadow="0 0 20px rgba(0, 0, 0, 0.05)"
            borderRadius={4}
          >
            asd
          </Box>
        </SimpleGrid>
      </Flex>
    </Flex>
  )
}