import {
  Checkbox,
  Flex,
  FormControl,
  FormLabel,
  Heading, Image,
  Link,
  Stack
} from '@chakra-ui/react';
import Background from '../assets/background.svg';
import PetzzLetters from '../assets/Petzz-letters.svg';
import { Button } from '../components/Button';
import { Input } from '../components/Form/Input';


export default function SplitScreen() {
  return (
    <Stack minH={'100vh'} direction={{ base: 'column', md: 'row' }}>
      <Flex maxW={400} p={8} flex={1} align={'center'} justify={'center'}>
        <Stack spacing={4} w={'full'} maxW={'md'}>
      <Image
        minWidth={'100%'}
        maxHeight={'100vh'}
          alt={'Login Image'}
          objectFit={'cover'}
          src={ PetzzLetters }
        />
          <Heading fontSize={'2xl'}>Entre na sua conta</Heading>
          <FormControl id="email">
            <FormLabel>Endereço de email</FormLabel>
            <Input name="email" type="email" />
          </FormControl>
          <FormControl id="password">
            <FormLabel>Senha</FormLabel>
            <Input name="password" type="password" />
          </FormControl>
          <Stack spacing={6}>
            <Stack
              direction={{ base: 'column', sm: 'row' }}
              align={'start'}
              justify={'space-between'}>
              <Checkbox>Remember me</Checkbox>
              <Link color={'blue.500'}>Forgot password?</Link>
            </Stack>
            <Button>
              Entrar
            </Button>
          </Stack>
        </Stack>
      </Flex>
      <Flex flex={1}>
        <Image
        minWidth={'100%'}
        maxHeight={'100vh'}
          alt={'Login Image'}
          objectFit={'cover'}
          src={ Background }
        />
      </Flex>
    </Stack>
  );
}