import { Box, Checkbox, Flex, Heading, HStack, Table, Tag, Tbody, Td, Text, Th, Thead, Tr } from '@chakra-ui/react'
import { RiAddLine, RiArrowDropDownLine } from 'react-icons/ri'
import { Button } from '../components/Button'
import { Header } from '../components/Header'
import { Sidebar } from '../components/Sidebar'


export default function Subscribers() {
  return (
    <Box>
      <Header />
      <Flex my="6">
        <Sidebar />
        <Box
          flex="1"
          mr="8"
          borderRadius={4}
          bgColor="white"
          shadow="0 0 20px rgba(0, 0, 0, 0.05)"
          p="8"
        >
          <Flex mb="8" justifyContent="space-between" alignItems="center">
            <Box>
              <Heading size="lg" fontWeight="medium">Clientes</Heading>
              <Text mt="1" color="gray.400">Listagem completa de clientes</Text>
            </Box>

            <HStack>
              <Button size="md" rightIcon={<RiArrowDropDownLine size="24" />} colorScheme="pink" disabled>
                Ações em massa
              </Button>

              <Button size="md" leftIcon={<RiAddLine size="24" />}>
                Adicionar
              </Button>
            </HStack>
          </Flex>

          <Table>
            <Thead>
              <Tr>
                <Th width="8">
                  <Checkbox />
                </Th>
                <Th>Contato</Th>
                <Th>Ultima compra</Th>
                <Th>Status</Th>
              </Tr>
            </Thead>
            <Tbody>
              <Tr>
                <Td>
                  <Checkbox />
                </Td>
                <Td>
                  <Box>
                    <Text fontWeight="medium" fontSize="sm">Antônio Silva</Text>
                    <Text color="gray.500" fontSize="small">tonybsilvadev@gmail.com</Text>
                  </Box>
                </Td>
                <Td color="gray.500">04 de Abril, 2021</Td>
                <Td>
                  <Tag colorScheme="green">Aberta</Tag>
                </Td>
              </Tr>
              <Tr>
                <Td>
                  <Checkbox />
                </Td>
                <Td>
                  <Box>
                    <Text fontWeight="medium" fontSize="sm">Antônio Silva</Text>
                    <Text color="gray.500" fontSize="small">tonybsilvadev@gmail.com</Text>
                  </Box>
                </Td>
                <Td color="gray.500">04 de Abril, 2021</Td>
                <Td>
                  <Tag colorScheme="yellow">Atrasada</Tag>
                </Td>
              </Tr>
              <Tr>
                <Td>
                  <Checkbox />
                </Td>
                <Td>
                  <Box>
                    <Text fontWeight="medium" fontSize="sm">Antônio Silva</Text>
                    <Text color="gray.500" fontSize="small">tonybsilvadev@gmail.com</Text>
                  </Box>
                </Td>
                <Td color="gray.500">04 de Abril, 2021</Td>
                <Td>
                  <Tag colorScheme="red">Quitada</Tag>
                </Td>
              </Tr>
            </Tbody>
          </Table>

          <Flex mt="8" justifyContent="space-between" alignItems="center">
            <Box>
              <Text fontSize="md" color="gray.600">
                <strong>1</strong> - <strong>10</strong> de <strong>48</strong>
              </Text>
            </Box>

            <HStack spacing="2">
              <Button size="md" width="4">1</Button>
              <Button size="md" width="4" bgColor="gray.300">2</Button>
              <Button size="md" width="4" bgColor="gray.300">3</Button>
              <Button size="md" width="4" bgColor="gray.300">4</Button>
              <Text color="gray.500" px="2">...</Text>
              <Button size="md" width="4" bgColor="gray.300">67</Button>
              <Button size="md" width="4" bgColor="gray.300">68</Button>
            </HStack>
          </Flex>
        </Box>
      </Flex>
    </Box>
  )
}